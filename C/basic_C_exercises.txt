// Colecci�n Ejercicios C

1. Mostrar por pantalla "Hola Mundo!", de color verde.
2. Pedir 2 n�meros al usuario y mostrar el producto.
3. Pedir 2 n�meros al usuario y determinar cual es el mayor.
4. Pedir 3 n�meros al usuario y determinar cual es el mayor.
5. Pedir 1 n�mero al usuario y mostrar su tabla de multiplicar.

6. Pedir 2 n�meros al usuario y mostrar el producto mediante sumas.
	Ejemplo: 8 x 3 = 8 + 8 + 8 = 24

7. Pedir 10 n�meros al usuario y mostrar la suma y el producto de ellos.

8. Pedir una secuencia de n�meros positivos y mostrar la suma de dichos n�meros.
	NOTA: Cuando se introduce un n�mero negativo, el programa deja de pedir n�meros y finaliza.

9. Pedir una secuencia de n�meros y mostrar su producto y la media. 
   Tras pedir cada n�mero, preguntar al usuario si desea finalizar: (y) o (n)
	NOTA: El programa finalizar� cuando el usuario pulse la tecla 'y'.

10. Pedir 1 n�mero y determinar su factorial, usar una funcion: CalculaFactorial().

PRO. Pedir 1 n�mero al usuario y mostrar su valor en binario (con 1s y 0s).

Puntos Extra:
� Si compila y funciona!
� Si esta correctamente tabulado y espaciado.
� Si esta detalladamente comentado.
� Si utiliza colores de pantalla, limpiar pantalla u otras funciones vistas en clase.
� Si al mostrar datos por pantalla estan bien ordenados con saltos de linea y demas.
� Si el programa incluye detalles inteligentes y divertidos.