//09. Write a program that asks for seconds and convert it into minutes, hours and days

#include <stdio.h>

int main ()
{
    int num1;
    int resultado;
    
    printf ("Pon un numero para saber su tabla de multiplicar:\n");
    scanf ("%i", &num1);
    getchar ();    
    
    printf ("La tabla de multiplicar es: \n");
    for (int i = 0; i<10; i++)
    {
        resultado = num1 * i;
        printf ("%i * %i = %i", num1, i, resultado);
    }
    
    getchar ();
    return 0;
}