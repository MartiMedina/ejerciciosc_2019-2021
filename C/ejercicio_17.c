//17. Write a program that prints five random numbers, like a Lottery.

#include <stdio.h>
#include <stdlib.h>

int main ()
{
    //Declararemos todo lo necesario para generar los numeros pseudoaleatorios
    int numeros = 5;
    time_t t;
    
    srand((unsigned) time (&t));
    
    printf ("Los numeros que te han tocado son: ");
    
    //Creamos un bucle de escritura de los numeros aleatorios
    for (int i = 0; i<numeros; i++)
    {
        printf ("%d ", rand()%50);
    }
    
   getchar();
   return 0;
}