#include <stdlib.h>
#include <stdio.h>
 
int main()
{
    char caracter = 'a';
    int entero = 10;
    float flotante = 1.5f;
    char cadena[] = "Jandu";
    double flotanteDeDoblePrecision = 3.14159;
   
   
    printf("El caracter es %c\n", caracter);
    printf("El entero es %d\n", entero);
    printf("El decimal es %f\n", flotante);
    printf("La cadena es %s\n", cadena);
    printf("El flotante de doble precision es %lf\n", flotanteDeDoblePrecision);
   
    getchar();
   
    return 0;
}