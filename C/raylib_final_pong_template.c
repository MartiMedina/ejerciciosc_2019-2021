/*******************************************************************************************
*
*   raylib game: FINAL PONG - game template
*
*   developed by [STUDENT NAME HERE]
*
*   This example has been created using raylib 1.0 (www.raylib.com)
*   raylib is licensed under an unmodified zlib/libpng license (View raylib.h for details)
*
*   Copyright (c) 2014 Ramon Santamaria (Ray San)
*
********************************************************************************************/

#include "raylib.h"

typedef enum GameScreen { LOGO =0 , TITLE, GAMEPLAY, ENDING, ENDING2 } GameScreen;

int main()
{
    // Initialization
    //--------------------------------------------------------------------------------------
    int screenWidth = 800;
    int screenHeight = 450;
    char windowTitle[30] = "raylib game - FINAL PONG";
    
    GameScreen currentScreen = LOGO;
    
    // TODO: Define required variables here..........................(0.5p)
    // NOTE: Here there are some useful variables (should be initialized)
    const int velocidady = 8;
    const int ballSize = 17;
    const int maxVelocity = 5;
    const int minVelocity = 4;
    
    
    
    int secondsCounter = 99;
    bool pause = false;
    
    int framesCounter = 0;  // General pourpose frames counter
    
    int gameResult = -1;        // 0 - Loose, 1 - Win, -1 - Not defined
    bool blink = true;
    float alpha = 0.0f;   
    

    Rectangle enemyLife;
    
    enemyLife.width = 100;
    enemyLife.height = 20;    
    enemyLife.x = screenWidth - 40 - enemyLife.width;
    enemyLife.y = screenHeight - 420 - enemyLife.height;
    
    Rectangle playerLife;
    
    playerLife.width = 100;
    playerLife.height = 20;    
    playerLife.x = 20;
    playerLife.y = screenHeight - 420 - playerLife.height;
    
    Rectangle paladerecha;
   
    paladerecha.width = 20;
    paladerecha.height = 80;    
    paladerecha.x = screenWidth - 40 - paladerecha.width;
    paladerecha.y = screenHeight/2 - paladerecha.height/2;
   
   
    Rectangle palaizquierda;
   
    palaizquierda.width = 20;
    palaizquierda.height = 80;    
    palaizquierda.x = 40;
    palaizquierda.y = screenHeight/2 - palaizquierda.height/2;
   
   
    Vector2 ball;
    ball.x = screenWidth/2;
    ball.y = screenHeight/2;
   
    Vector2 ballVelocity;
    ballVelocity.x = minVelocity;
    ballVelocity.y = minVelocity;
    
    int iaLinex = screenWidth/2;
    
    InitWindow(screenWidth, screenHeight, windowTitle);
    
    // NOTE: If using textures, declare Texture2D variables here (after InitWindow)
    Texture2D title = LoadTexture("Resources/pong_title_texture.png");
    // NOTE: If using SpriteFonts, declare SpriteFont variables here (after InitWindow)
    Font fontTtf = LoadFontEx("Resources/Gameplay.ttf", 32, 0, 250);
    Font fontTtf2 = LoadFontEx("Resources/Mandatory.otf", 32, 0, 250);
    // NOTE: If using sound or music, InitAudioDevice() and load Sound variables here (after InitAudioDevice)
    InitAudioDevice();      // Initialize audio device
    // Load sound
    Sound fxWav = LoadSound("Resources/bounce.wav");         // Load WAV audio file
    Sound fxGoal = LoadSound("Resources/goal.wav");
    
    SetTargetFPS(60);
    //--------------------------------------------------------------------------------------
    
    // Main game loop
    while (!WindowShouldClose())    // Detect window close button or ESC key
    
      {  // Update
        //----------------------------------------------------------------------------------
        switch(currentScreen) 
        {
            case LOGO: 
            {
                // Update LOGO screen data here!
               if (framesCounter >= 0)
               {
                   alpha += 0.01f;
               
               }
               
               if (framesCounter >= 150)
               {
                    alpha -= 0.02f;
               }
                // TODO: Logo fadeIn and fadeOut logic...............(0.5p)
                
                framesCounter++;

            if (framesCounter> 300)       // Every 12 frames, one more letter!
            {
                 currentScreen = TITLE;
            }
             
            } break;
            
            case TITLE: 
            {
                // Update TITLE screen data here!
                
                // TODO: Title animation logic.......................(0.5p)
                if (framesCounter >= 300)
               {
                   alpha += 0.05f;
               
               }
                 framesCounter++;
                if (framesCounter%20  == 0)
                {
                blink = !blink;
                framesCounter = 0;
                }
                // TODO: "PRESS ENTER" logic.........................(0.5p)
                if (IsKeyPressed(KEY_ENTER))
                 {
                 currentScreen = GAMEPLAY;
                 }
                    
            } break;
            case GAMEPLAY:
            { 
                // Update GAMEPLAY screen data here!

                // TODO: Ball movement logic.........................(0.2p)
                    if(!pause){
                        ball.x += ballVelocity.x;
                        ball.y += ballVelocity.y;
                    }
                // TODO: Player movement logic.......................(0.2p)
                if(!pause){
                    if (IsKeyDown(KEY_Q)){
                      palaizquierda.y -= velocidady;
                       //iaLinex+=velocidady;
                    }
                   
                    if (IsKeyDown(KEY_A)){
                        //iaLinex -=velocidady;
                      palaizquierda.y += velocidady;
                    }                       
                    // TODO: Enemy movement logic (IA)...................(1p)
                    if (IsKeyDown(KEY_RIGHT)){
                        iaLinex+=velocidady;
                    }
                   
                    if (IsKeyDown(KEY_LEFT)){
                        iaLinex -=velocidady;
                    }
                }
               
                
                // TODO: Collision detection (ball-enemy) logic......(0.5p)
                if(CheckCollisionCircleRec(ball, ballSize, paladerecha)){
                    if(ballVelocity.x>0){
                        PlaySound(fxWav);
                        if(abs(ballVelocity.x)<maxVelocity){                    
                            ballVelocity.x *=-1.1;
                            ballVelocity.y *= 1.1;
                        }else{
                            ballVelocity.x *=-1;
                        }
                    }
                }
                // TODO: Collision detection (ball-player) logic.....(0.5p)
                if(CheckCollisionCircleRec(ball, ballSize, palaizquierda)){
                    if(ballVelocity.x<0){
                        PlaySound(fxWav);
                        if(abs(ballVelocity.x)<maxVelocity){                    
                            ballVelocity.x *=-1.1;
                            ballVelocity.y *= 1.1;
                        }else{
                            ballVelocity.x *=-1;
                        }
                    }
                }
              
                // TODO: Collision detection (ball-limits) logic.....(1p)
                if((ball.y > screenHeight - ballSize) || (ball.y < ballSize) ){
                    ballVelocity.y *=-1;
                    PlaySound(fxWav);
                }
                
                   if( ball.x > iaLinex){
                    if(ball.y > paladerecha.y){
                        paladerecha.y+=velocidady;
                    }
                   
                    if(ball.y < paladerecha.y){
                        paladerecha.y-=velocidady;
                    }
                }
                    if(ball.x > screenWidth - ballSize){
                    //Marca la pala izquierda
                    enemyLife.width-=10;
                    PlaySound(fxGoal);
                    ball.x = screenWidth/2;
                    ball.y = screenHeight/2;
                    ballVelocity.x = -minVelocity;
                    ballVelocity.y = minVelocity;
                   
                    }else if(ball.x < ballSize){
                    //Marca la pala derecha
                    playerLife.width-=10;
                    PlaySound(fxGoal);
                    ball.x = screenWidth/2;
                    ball.y = screenHeight/2;
                    ballVelocity.x = minVelocity;
                    ballVelocity.y = minVelocity;
                    }
                // TODO: Life bars decrease logic....................(1p)
                   if (playerLife.width==0) 
                   {
                       currentScreen = ENDING;
                       
                   }
                   if (enemyLife.width==0) 
                   {
                       currentScreen = ENDING2;
                       
                   }
                // TODO: Time counter logic..........................(0.2p)
                
                // TODO: Game ending logic...........................(0.2p)
              
                 
                   
                // TODO: Pause button logic..........................(0.2p)
              if (IsKeyPressed(KEY_P)){
                  pause = !pause;
                }
               
                //Gestionamos el movimiento de la Bola
                if(!pause){
                    ball.x += ballVelocity.x;
                    ball.y += ballVelocity.y;
                }
            } break;
            case ENDING: 
            {
                // Update END screen data here!
                
                // TODO: Replay / Exit game logic....................(0.5p)
                
            } break;
            case ENDING2: 
            {
                // Update END screen data here!
                
                // TODO: Replay / Exit game logic....................(0.5p)
                
            } break;
            default: break;
        }
        //----------------------------------------------------------------------------------
        
        // Draw
        //----------------------------------------------------------------------------------
        BeginDrawing();
        
            ClearBackground(BLACK); 
            
            
            switch(currentScreen) 
            {
                case LOGO: 
                {
                    // Draw LOGO screen here!
                    DrawTextEx(fontTtf, "P_0_N_G", (Vector2){screenWidth/2 - 10 - MeasureText("P_0_N_G", 30)/2, screenHeight/2 - 20}, 40, 5, Fade(LIGHTGRAY, alpha));
                    
                    // TODO: Draw Logo...............................(0.2p)
                    
                } break;
                case TITLE: 
                {
                    // Draw TITLE screen here!
                    DrawTexture(title, 0, 0, WHITE);
                    // TODO: Draw Title..............................(0.2p)
                    DrawTextEx(fontTtf2, "The P0NG", (Vector2){screenWidth/2 - 65 - MeasureText("The P0NG", 30)/2, screenHeight/2 - 90}, 50, 5, Fade(GREEN, alpha));
                    // TODO: Draw "PRESS ENTER" message..............(0.2p)
                    if (blink) DrawText("Press enter to play", (screenWidth +415 - MeasureText("Press enter to play", 60))/2, screenHeight/2 , 20, GREEN);
                    if (blink) DrawText("Press enter to play", (screenWidth +417 - MeasureText("Press enter to play", 60))/2, screenHeight/2 , 20, LIGHTGRAY);
                } break;
                case GAMEPLAY:
                { 
                    ClearBackground(GOLD); 
                    // Draw GAMEPLAY screen here!
                    DrawRectangleRec(paladerecha, RED);
                    // TODO: Draw player and enemy...................(0.2p)
                    // Pinto Pala Izquierda
                    DrawRectangleRec(palaizquierda, BLUE);
           
                    // Pintamos la pelota
                    DrawCircleV(ball, ballSize, VIOLET);  
           
                    // Pintar linea IA
                    DrawLine(iaLinex, 0, iaLinex , screenHeight, BLACK);                      
                  
                    
                    // TODO: Draw player and enemy life bars.........(0.5p)
                    DrawRectangleRec(enemyLife, GREEN);
                    DrawRectangleRec(playerLife, GREEN);
                    // TODO: Draw time counter.......................(0.5p)
                    
                    // TODO: Draw pause message when required........(0.5p)
                   if(pause){
                        DrawRectangle(0, 0, screenWidth, screenHeight, (Color){ 0, 255, 0, 255/2 });  
                    DrawTextEx(fontTtf2, "Press p to continue", (Vector2){screenWidth/2 - MeasureText("Press p to continue", 30)/2 -50, screenHeight/2}, 30, 5, VIOLET);
                    }
                    
                    
                } break;
                case ENDING: 
                {
                    // Draw END screen here!
                    
                     
                     DrawRectangle(0, 0, screenWidth, screenHeight, (Color){ 0, 255, 0, 255/2 });  
                     DrawTextEx(fontTtf2, "YOU LOOSE", (Vector2){screenWidth/2 - MeasureText("YOU LOOSE", 30)/2 -50, screenHeight/2}, 30, 5, BLACK);
                    
                    // TODO: Draw ending message (win or loose)......(0.2p)
                    
                } break;
                case ENDING2: 
                {
                    // Draw END screen here!
                    
                     
                     DrawRectangle(0, 0, screenWidth, screenHeight, (Color){ 0, 255, 0, 255/2 });  
                     DrawTextEx(fontTtf2, "YOU WIN", (Vector2){screenWidth/2 - MeasureText("YOU WIN", 30)/2 -50, screenHeight/2}, 30, 5, BLACK);
                    
                    // TODO: Draw ending message (win or loose)......(0.2p)
                    
                } break;
                default: break;
            }
        
            DrawFPS(10, 10);
        
        EndDrawing();
        //----------------------------------------------------------------------------------
    }

    // De-Initialization
    //--------------------------------------------------------------------------------------
    
    // NOTE: Unload any Texture2D or SpriteFont loaded here
    UnloadTexture(title);
    
    CloseWindow();        // Close window and OpenGL context
    //--------------------------------------------------------------------------------------
    
    return 0;
}